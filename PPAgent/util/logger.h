#pragma once
#include "../pch.h"
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup.hpp>
#include <stdio.h>
#include <stdarg.h>

namespace wa
{
	class Logger
	{
	private:
		char _buffer[LOGGER_BUFFER_SIZE];
	public:
		static Logger& getInstance()
		{
			static Logger instance; // Guaranteed to be destroyed.
			return instance;
		}

		Logger()
		{								
			boost::log::add_file_log(
				boost::log::keywords::file_name = LOGGING_FILE_NAME,
				boost::log::keywords::rotation_size = 1 * 1024 * 1024,
				boost::log::keywords::max_size = 20 * 1024 * 1024,
				boost::log::keywords::time_based_rotation = boost::log::sinks::file::rotation_at_time_point(0, 0, 0),
				boost::log::keywords::format = LOGGING_FORMAT(boost::log, boost::log::expressions),
				boost::log::keywords::auto_flush = true
			);
			LOGGING_FILTER(boost::log, LOGGING_RULE(boost::log))					
			boost::log::core::get()->add_global_attribute("TimeStamp", boost::log::attributes::local_clock());
			//boost::log::attributes::
		}
		~Logger()
		{

		}

		void trace(const char *fmt, ...)
		{											
			LOGGING_MAKE_BUFFER(_buffer, fmt);
			BOOST_LOG_TRIVIAL(trace) << _buffer;
		}

		void debug(const char *fmt, ...)
		{
			LOGGING_MAKE_BUFFER(_buffer, fmt);				
			BOOST_LOG_TRIVIAL(debug) << _buffer;
		}

		void info(const char *fmt, ...)
		{
			LOGGING_MAKE_BUFFER(_buffer, fmt);
			BOOST_LOG_TRIVIAL(info) << _buffer;
		}

		void warning(const char *fmt, ...)
		{
			LOGGING_MAKE_BUFFER(_buffer, fmt);
			BOOST_LOG_TRIVIAL(warning) << _buffer;
		}

		void error(const char *fmt, ...)
		{
			LOGGING_MAKE_BUFFER(_buffer, fmt);
			BOOST_LOG_TRIVIAL(error) << _buffer;
		}

		void fatal(const char *fmt, ...)
		{
			LOGGING_MAKE_BUFFER(_buffer, fmt);
			BOOST_LOG_TRIVIAL(fatal) << _buffer;
		}		
	};
}