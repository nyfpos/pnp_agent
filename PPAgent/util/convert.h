#pragma once
#include <iostream>
namespace wa
{
	class Boolean
	{
		bool boolVal;
	public:		
		Boolean() :boolVal(false) {}
		Boolean(bool data) :boolVal(data){}
		Boolean(std::string data)
		{
			if (data == std::string("0"))
				boolVal = false;
			else
				boolVal = true;
		}
		Boolean(int data)
		{
			boolVal = data == 0 ? false : true;
		}
		operator int() const
		{
			return boolVal ? 1 : 0;
		}
		operator bool() const
		{
			return boolVal;
		}
		operator std::string() const
		{
			return boolVal ? std::string("1") : std::string("0");
		}
	};
}