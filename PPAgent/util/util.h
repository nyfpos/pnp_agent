#pragma once
#include "..\pch.h"
#include "soci.h"
#include "soci-odbc.h"

#include <algorithm>
#include <sstream>
#include <iterator>

#include <excpt.h>

//printf("[%d] %s %d\n", i, props.get_name().c_str(), props.get_data_type());
#define GetValidRowData(T, index, row, val)\
{\
	int i = index;\
    const soci::column_properties & props = row.get_properties(i);\
    bool beNullVal = false;\
    if (row.get_indicator(i) != soci::i_null)\
        val = row.get<T>(i);\
}

namespace wa
{
	namespace util
	{
		template<typename T>
		std::string vectorToString(const char* del, std::vector<T>& vec)
		{
			std::ostringstream oss;

			if (!vec.empty())
			{
				std::copy(vec.begin(), vec.end() - 1,
					std::ostream_iterator<T>(oss, del));
				oss << vec.back();
			}
			return oss.str();
		}
	}
}