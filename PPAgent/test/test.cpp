#include "test.h"

#include <fstream>
#include <sstream>
#include <string>

namespace wa
{
	void gen_random(char *s, const int len) {
		static const char alphanum[] =
			"0123456789"
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz";

		for (int i = 0; i < len; ++i) {
			s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
		}

		s[len] = 0;
	}

	/*
	int arrivedcount = 0;
	volatile int toStop = 0;
	void cfinish(int sig)
	{
		signal(SIGINT, NULL);
		toStop = 1;
	}

	void messageArrived(MQTT::MessageData& md)
	{
		MQTT::Message &message = md.message;
		printf("[Qos:%d topic:%.*s] %.*s\n", message.qos, md.topicName.lenstring.len, md.topicName.lenstring.data, (int)message.payloadlen, (char*)message.payload);
	}

	BOOL WINAPI ConsoleHandlerRoutine(DWORD dwCtrlType)
	{
		if (dwCtrlType == CTRL_CLOSE_EVENT)
		{
			toStop = 1;
			return TRUE;
		}

		return FALSE;
	}

	int connectIPStack(CppWebsock& ipstack, const char *address, int port, int use_ssl, const char *path, const char *host, const char *origin, int ietf_version)
	{
		int rc = ipstack.connect(address, port, use_ssl, path, address, address, ietf_version);
		while (!ipstack.isConnected())
		{
			if (ipstack.isConnectFailed() || rc == -1)
			{
				Sleep(10);
				printf("Retry Connecting to %s:%d\n", address, port);
				rc = ipstack.connect(address, port, use_ssl, path, address, address, ietf_version);
			}
			Sleep(1);
		}
		return rc;
	}

	void mqtt_test1()
	{
		unsigned seed = (unsigned)time(NULL);
		srand(seed);

		char clientID[50] = { 0 };
		char randstr[11] = { 0 };
		gen_random(randstr, 10);
		sprintf_s(clientID, "%s-%s", "clientId", randstr);
		printf("connection client: %s\n", clientID);

		SetConsoleCtrlHandler(ConsoleHandlerRoutine, TRUE);
		int n = 0;
		int ret = 0;
		int port = 7681;
		int use_ssl = 2;
		const char *address;
		int ietf_version = -1; 
		char* path;

		signal(SIGINT, cfinish);
		signal(SIGTERM, cfinish);

		char** argv = new char*[4]
		{
			NULL,
			"172.18.3.157",//"wavmrd.cloudapp.net",
			"443",
			"/WaMQTT/"//"/MQTT/"
		};
		int optind = 1;
		address = argv[optind];
		port = atoi(argv[optind + 1]);
		path = argv[optind + 2];

		CppWebsock ipstack = CppWebsock();
		float version = 0.3;
		char* topic = "test0";

		printf("Version is %f\n", version);

		MQTT::Client<CppWebsock, Countdown> client = MQTT::Client<CppWebsock, Countdown>(ipstack);
		CppWebsock::start();

		//	const char* hostname = "broker.mqttdashboard.com";
		//	int port = 1883;
		printf("Connecting to %s:%d\n", address, port);
		int rc = connectIPStack(ipstack, address, port, use_ssl, path, address, address, ietf_version);

		printf("rc from TCP connect is %d\n", rc);
		printf("MQTT connecting\n");
		MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
		data.MQTTVersion = 3;
		data.clientID.cstring = clientID;
		data.username.cstring = "admin";
		data.password.cstring = "admin";
		rc = client.connect(data);
		if (rc != 0) printf("rc from MQTT connect is %d\n", rc);
		printf("MQTT connected\n");
		rc = client.subscribe(topic, MQTT::QOS1, messageArrived);
		if (rc != 0) printf("rc from MQTT subscribe is %d\n", rc);
		//int count = 100000;
		time_t prev = time(NULL);
		while (!toStop) //&& count > 0)
		{
			if (!ipstack.isConnected())
			{
				rc = connectIPStack(ipstack, address, port, use_ssl, path, address, address, ietf_version);
				if (rc != 0) printf("rc from TCP connect is %d\n", rc);
				rc = client.disconnect();
				if (rc != 0) printf("rc from disconnect was %d\n", rc);
				printf("MQTT connecting\n");
				rc = client.connect(data);
				if (rc != 0) printf("rc from MQTT connect is %d\n", rc);
				rc = client.subscribe(topic, MQTT::QOS1, messageArrived);
				if (rc != 0) printf("rc from MQTT subscribe is %d\n", rc);
			}
			client.yield(100);
			double second = difftime(time(NULL), prev);
			if (second > 10)
			{
				const int SIZE = 50;
				char payload[SIZE] = { 0 };
				srand(time(NULL));
				sprintf_s(payload, "%s say (%d)...", clientID, rand() % 100 + 1);
				MQTT::Message msg;
				msg.qos = MQTT::QOS1;
				msg.id = msg.retained = msg.dup = 0;
				msg.payload = payload;
				msg.payloadlen = strlen((char*)msg.payload);
				client.publish(topic, msg);
				prev = time(NULL);
			}
			//count--;
			Sleep(1);
		}		


		rc = client.unsubscribe(topic);
		if (rc != 0) printf("rc from unsubscribe was %d\n", rc);

		rc = client.disconnect();
		if (rc != 0) printf("rc from disconnect was %d\n", rc);

		CppWebsock::shutdown();
		delete[] argv;
	}
	*/
	// ******************************* //
	
	char mqtt_clientID[50] = { 0 };
	//char* mqtt_topic = "test0";	
	char* mqtt_subscribe_topic = "iot-2/evt/wacfg/fmt/#";
	char* mqtt_publish_topic = "iot-2/evt/wacfg/fmt/test0";


	void mqtt_message_recv(MQTT::MessageData &md)
	{
		MQTT::Message &message = md.message;
		//printf("[Qos:%d topic:%.*s] %.*s\n", message.qos, md.topicName.lenstring.len, md.topicName.lenstring.data, (int)message.payloadlen, (char*)message.payload);

		char randstr[11] = { 0 };
		gen_random(randstr, 10);
		std::string filename = std::string("./tmp/")+ std::string(randstr) + std::string(".json");
		std::ofstream myfile;
		myfile.open(filename.c_str());
		myfile << (char*)message.payload;
		myfile.close();
		printf("create file %s done\n", filename.c_str());
	}

	void mqtt_message_pulish(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
	{
		MqttManager& mqttMgr = MqttManager::getInstance();	
		/*
		const int SIZE = 50;
		char payload[SIZE] = { 0 };
		srand(time(NULL));	
		sprintf_s(payload, "%s say (%d)...", mqtt_clientID, rand() % 100 + 1);		
		*/

		std::string file;
		std::string line;
		std::ifstream myfile("test.json");
		if (myfile.is_open())
		{
			while (std::getline(myfile, line))
			{
				file = file + line;				
			}
			myfile.close();
		}
		else printf("Unable to open file\n");

		unsigned char* payload = (unsigned char*)file.c_str();
		int len = strlen((char*)payload);
//		printf("%s\n", payload);
		mqttMgr.publish(mqtt_publish_topic, payload);
	}

	void mqtt_connected(const MQTTConfig* const config)
	{
		if (config)
		{
			Logger::getInstance().info("[MQTT] connected(%s:%d%s)", config->host, config->port, config->path);
			MqttManager& mqttMgr = MqttManager::getInstance();
			int rc = mqttMgr.subscribe(mqtt_subscribe_topic, MQTT::QOS1, boost::bind(mqtt_message_recv, _1));
			if (rc == 0)
				printf("suscribe topic: %s successfully\n", mqtt_subscribe_topic);
			else
				printf("suscribe topic: %s failed\n", mqtt_subscribe_topic);

		}
	}

	void mqtt_disconnected(const MQTTConfig* const config)
	{
		if (config)
			Logger::getInstance().info("[MQTT] disconnected(%s:%d%s)", config->host, config->port, config->path);
	}

	void mqtt_test2()
	{		
//		Logger::getInstance().debug("[mqtt_test2] start");

		char randstr[11] = { 0 };
		srand(time(NULL));
		gen_random(randstr, 10);
		sprintf_s(mqtt_clientID, "%s-%s", "clientId", randstr);
		printf("connection client: %s\n", mqtt_clientID);

		MQTTConfig config = {
			"127.0.0.1",
			"/WaMQTT/",
			443,
			2,
			mqtt_clientID,
			"admin",
			"admin"
		};		

		MqttManager& mqttMgr = MqttManager::getInstance();
		mqttMgr.setDisconnectedCallback(boost::bind(mqtt_disconnected, _1));
		mqttMgr.setConnectedCallback(boost::bind(mqtt_connected, _1));
		mqttMgr.start(MQTT_TIMER_INTERVAL);		
		mqttMgr.connect(config);		


		SetTimer(NULL, TIMERID_TEST, 10 * 1000, (TIMERPROC)mqtt_message_pulish);
//		Logger::getInstance().debug("[mqtt_test2] end");
	}

	void logging_test()
	{
		Logger::getInstance().debug("Hi! My name is %s", "John");
	}


	/*
	bool get_name(string &name)
	{
		cout << "Enter name: ";
		return cin >> name;
	}
	*/
	void odbc_test()
	{
		bool okConn = false;
		soci::session sql;

		try
		{			
			sql.open(soci::odbc, "DSN=bwdb_Access;");
			okConn = true;
		}
		catch (std::exception const &e)
		{
			std::cerr << "Failure to open odbc. Error: " << e.what() << '\n';
			okConn = false;
		}

		if (okConn)
		{
			soci::transaction tr(sql);
			soci::row r;
			try
			{			
				soci::row row;
				soci::statement st = (sql.prepare << "select ProjIdbw, ProjName, ProjDesc, ProjIP, ProjPort from pProject", soci::into(row));
				st.execute();
				while (st.fetch())
				{					
					printf("ProjIdbw:%d, ProjName:%s, ProjDesc:%s, ProjIP:%s, ProjPort:%d\n", row.get<int>(0), row.get<std::string>(1).c_str(),
						                                                                      row.get<std::string>(2).c_str(), row.get<std::string>(3).c_str(), row.get<int>(4));
				}
				tr.commit();
			}
			catch (std::exception const &e)
			{
				std::cerr << "Error: " << e.what() << '\n';
				tr.rollback();
			}
		}
	}

	void mproj_test()
	{
		QProject q = {
			std::string("WADEMO")
		};
		MProject<QProject> mp(&q);
		ProjectData data;
		bool rs = mp.Get(data);		
		printf("ProjIdbw:%d, ProjName:%s, ProjDesc:%s, ProjIP:%s, ProjPort:%d\n", data.ProjIdbw, data.ProjName.c_str(), data.ProjDesc.c_str(), data.ProjIP.c_str(), data.ProjPort);
		printf("mproj_test %s\n", rs ? "success" : "fail");
	}

	void mproj_test2()
	{

		MProject<QProject> mp(&(QProject()));
		ProjectData data;

		std::string empty = std::string("");
		data.ProjName = std::string("WADEMO2");
		data.ProjDesc = std::string("TESTING SQL");
		data.ProjPath = std::string(".");
		data.ProjIP = std::string("127.0.0.1");
		data.ProjPort = 0;
		data.ProjTimeOut = 0;
		data.AccessSecurityCode = empty;
		data.ReservedText1 = data.ReservedText2 = empty;
		data.ReservedInt1 = data.ReservedInt2 = 0;

		bool rs = mp.Insert(data);
		printf("mproj_test2 %s\n", rs ? "success" : "fail");
	}

	void mscada_test()
	{
		QScada q = {
			1,
			std::string("Node")
		};

		MScada<QScada> mp(&q);
		ScadaData data;
		bool rs = mp.Get(data);
		printf("scada_name:%s scada_descr:%s\n", data.NodeName.c_str(), data.NodeDesc.c_str());
		printf("mscada_test %s\n", rs ? "success" : "fail");
	}

	void mscada_test2()
	{
		MScada<QScada> mp(&(QScada()));
		ScadaData data;
		data.ProjIdbw = 1;
		data.NodeName = std::string("SociTestNode");
		data.Address = std::string("127.0.0.1");
		data.IPList = std::string("1;172.18.3.14;");
		data.expand = true;
		bool rs = mp.Insert(data);		
		printf("mscada_test2 %s\n", rs ? "success" : "fail");
	}

	void mcomport_test()
	{
		QComport q;
		q.ProjNodeIdbw = 1;
		q.ComportNbr = 5;
		MComport<QComport> mp(&q);
		ComportData data;
		bool rs = mp.Get(data);
		printf("ComportIdbw:%d, InterfaceName:%s, Description:%s expand:%d \n", data.ComportIdbw, data.InterfaceName.c_str(), data.Description.c_str(), (int)data.Expand);

		ComportData data2;
		q.ComportNbr = 60;
		rs = mp.Get(data2);
		printf("ComportIdbw:%d, InterfaceName:%s, Description:%s expand:%d \n", data2.ComportIdbw, data2.InterfaceName.c_str(), data2.Description.c_str(), (int)data2.Expand);
		printf("===========================================================\n\n");

		std::vector<boost::shared_ptr<ComportData> >vData;		
		rs = mp.Get(vData);

		int i = 0;
		for (std::vector<boost::shared_ptr<ComportData> >::iterator it = vData.begin(); it != vData.end(); it++, i++)
		{
			boost::shared_ptr<ComportData>ptr = *it;
			printf("[%d] ComportIdbw:%d, InterfaceName:%s, Description:%s expand:%d \n", i, ptr->ComportIdbw, ptr->InterfaceName.c_str(), ptr->Description.c_str(), (int)ptr->Expand);
		}

		printf("mcomport_test %s\n", rs ? "success" : "fail");
	}

	void mcomport_test2()
	{
		MComport<QComport> mp(&(QComport()));
		ComportData data;

		data.ProjIdbw = 1;
		data.ProjNodeIdbw = 1;
		data.ComportIdbw = 7;
		data.InterfaceName = std::string("TCPIP");
		data.ComportNbr = 6;
		data.Description = std::string("test from PPAgent project.(1)");

		bool rs = mp.Insert(data);
		printf("mcomport_test2 %s\n", rs ? "success" : "fail");

		data.ComportNbr = 7;
		data.Description = std::string("test from PPAgent project.(2)");
		rs = mp.Insert(data);
		printf("mcomport_test2 %s\n", rs ? "success" : "fail");
	}

	void mcomport_test3()
	{
		QComport q;
		q.ProjNodeIdbw=	1;
		q.ComportNbr = 0;		
		MComport<QComport> mp(&q);
		std::vector<int> nbrs;
		bool rs = mp.GetUsedComport(nbrs);

		for (std::vector<int>::iterator it = nbrs.begin();
			it != nbrs.end(); it++)
		{
			printf("%d,", *it);
		}
		printf("\n");
		printf("mcomport_test3 %s\n", rs ? "success" : "fail");
	}

	void mcomport_test4()
	{
		QComport q;
		q.ProjNodeIdbw = 1;
		q.ComportNbr = 4;
		MComport<QComport> mp(&q);
		bool rs = mp.Delete();
		printf("mcomport_test4 %s\n", rs ? "success" : "fail");
	}

	void mcomport_test5()
	{
		QComport q;
		q.ProjNodeIdbw = 1;
		q.ComportNbrs.push_back(6);
		q.ComportNbrs.push_back(7);
		MComport<QComport> mp(&q);
		bool rs = mp.Delete();
		printf("mcomport_test5 %s\n", rs ? "success" : "fail");
	}

	void mdevice_test()
	{
		int success = 0, fail = 0;
		QDevice q;
		q.DeviceName = "Modbus";
		q.ComportIdbw = 13;
		MDevice<QDevice> mp(&q);
		DeviceData data;
		bool rs = mp.Get(data);
		printf("device_id:%d comport_no:%d device_name:%s device_type:%s\n", data.DeviceIdbw, data.ComportNbr, data.DeviceName.c_str(), data.DeviceType.c_str());		
		rs ? ++success : ++fail;

		printf("==============================\n\n");

		std::vector<boost::shared_ptr<DeviceData> > vData;
		q.ProjIdbw = q.ProjNodeIdbw = 1;
		rs = mp.Get(vData);

		for (std::vector<boost::shared_ptr<DeviceData> >::iterator it = vData.begin();
			it != vData.end();
			it++)
		{
			DeviceData& tmp = **it;
			printf("device_id:%d comport_no:%d device_name:%s device_type:%s\n", tmp.DeviceIdbw, tmp.ComportNbr, tmp.DeviceName.c_str(), tmp.DeviceType.c_str());
		}
		rs ? ++success : ++fail;

		printf("==============================\n\n");
			
		vData.clear();
		rs = mp.Get("WADEMO", "Node", vData);

		for (std::vector<boost::shared_ptr<DeviceData> >::iterator it = vData.begin();
			it != vData.end();
			it++)
		{
			DeviceData& tmp = **it;
			printf("device_id:%d comport_no:%d device_name:%s device_type:%s\n", tmp.DeviceIdbw, tmp.ComportNbr, tmp.DeviceName.c_str(), tmp.DeviceType.c_str());
		}
		rs ? ++success : ++fail;
		printf("==============================\n\n");

		vData.clear();
		rs = mp.Get(q.ComportIdbw, vData);

		for (std::vector<boost::shared_ptr<DeviceData> >::iterator it = vData.begin();
			it != vData.end();
			it++)
		{
			DeviceData& tmp = **it;
			printf("device_id:%d comport_no:%d device_name:%s device_type:%s\n", tmp.DeviceIdbw, tmp.ComportNbr, tmp.DeviceName.c_str(), tmp.DeviceType.c_str());
		}
		rs ? ++success : ++fail;
		printf("mdevice_test success:%d fail:%d", success, fail);
	}

	void mdevice_test2()
	{
		QDevice q;
		q.ProjNodeIdbw = 1;
		q.DeviceIdbw = 18;
		MDevice<QDevice> mp(&q);
		bool rs = mp.Delete();
		printf("mdevice_test2 %s\n", rs ? "success" : "fail");
	}

	void mdevice_test3()
	{
		DeviceData data;
		data.ProjIdbw = 1;
		data.ProjNodeIdbw = 1;
		data.ComportIdbw = 11;
		data.DeviceIdbw = 10;
		data.ComportNbr = 1;
		data.DeviceName = "DEVICEA_1";
		data.DeviceType = "WAMQTT";
		MDevice<QDevice> mp(&(QDevice()));
		bool rs = mp.Insert(data);
		printf("mdevice_test3 %s\n", rs ? "success" : "fail");
	}

	void mtanalog_test()
	{		 
		 std::vector<boost::shared_ptr<TAnalogData> > vData;
		 QTag q;
		 q.ProjNodeIdbw = 1;
		 q.DeviceIdbw = 4;
		 MTAnalog<QTag> mp(&q);
		 bool rs = mp.Get(vData);
		 		
		 for (std::vector<boost::shared_ptr<TAnalogData> >::iterator it = vData.begin();
			 it != vData.end(); it++)
		 {
			 TAnalogData& data = **it;
			 printf("[mtanalog_test] TagName:%s Value:%lf SpanHigh:%lf SpanLow:%lf\n", data.TagName.c_str(), data.Value, data.SpanHigh, data.SpanLow);
		 }

		 printf("mtanalog_test %s\n", rs ? "success" : "fail");
	}
}