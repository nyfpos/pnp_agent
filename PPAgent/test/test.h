#pragma once
#include "..\pch.h"

/*
#include "..\lib\mqtt\CppWebsock.h"
#include "..\lib\mqtt\MQTTClient.h"
*/

#include "..\net\WaMqttManager.h"

#include "..\models\MProject.h"
#include "..\models\MScada.h"
#include "..\models\MComport.h"
#include "..\models\MDevice.h"
#include "..\models\MTAnalog.h"

namespace wa
{
	void mqtt_test1();
	void mqtt_test2();
	void logging_test();
	void odbc_test();
	void mproj_test();
	void mproj_test2();
	void mscada_test();
	void mscada_test2();
	void mcomport_test();
	void mcomport_test2();
	void mcomport_test3();
	void mcomport_test4();
	void mcomport_test5();
	void mdevice_test();
	void mdevice_test2();
	void mdevice_test3();

	void mtanalog_test();

}