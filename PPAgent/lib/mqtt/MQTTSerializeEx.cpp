#include "MQTTSerializeEx.h"

int MQTTSerialize_connectLengthEx(MQTTPacket_connectData* options)
{
	int len = 0;

	if (options->MQTTVersion == 3)
		len = 12; /* variable depending on MQTT or MQIsdp */
	else if (options->MQTTVersion == 4)
		len = 10;

	len += MQTTstrlen(options->clientID) + 2;
	if (options->willFlag)
		len += MQTTstrlen(options->will.topicName) + 2 + MQTTstrlen(options->will.message) + 2;
	if (options->username.cstring || options->username.lenstring.data)
		len += MQTTstrlen(options->username) + 2;
	if (options->password.cstring || options->password.lenstring.data)
		len += MQTTstrlen(options->password) + 2;

	return len;
}

int MQTTSerialize_subscribeLengthEx(int count, MQTTString topicFilters[])
{
	int i;
	int len = 2; /* packetid */

	for (i = 0; i < count; ++i)
		len += 2 + MQTTstrlen(topicFilters[i]) + 1; /* length + topic + req_qos */
	return len;
}

int MQTTSerialize_unsubscribeLengthEx(int count, MQTTString topicFilters[])
{
	int i;
	int len = 2; /* packetid */

	for (i = 0; i < count; ++i)
		len += 2 + MQTTstrlen(topicFilters[i]); /* length + topic*/
	return len;
}

int MQTTSerialize_publishLengthEx(int qos, MQTTString topicName, int payloadlen)
{
	int len = 0;

	len += 2 + MQTTstrlen(topicName) + payloadlen;
	if (qos > 0)
		len += 2; /* packetid */
	return len;
}

int MQTTSerialize_ack_ex(MQTTStream& stream, unsigned char packettype, unsigned char dup, unsigned short packetid)
{
	stream.clear();
	const int SIZE = 4;
	stream.capacity(SIZE);
	return MQTTSerialize_ack(stream.begin(), SIZE, packettype, dup, packetid);
}

int MQTTSerialize_pingreq_ex(MQTTStream& stream)
{
	stream.clear();
	const int SIZE = 2;
	stream.capacity(SIZE);
	return MQTTSerialize_pingreq(stream.begin(), SIZE);
}

int MQTTSerialize_connect_ex(MQTTStream& stream, MQTTPacket_connectData* options)
{
	stream.clear();
	int len = MQTTPacket_len(MQTTSerialize_connectLengthEx(options));
	stream.capacity(len);
	return MQTTSerialize_connect(stream.begin(), len, options);
}

int MQTTSerialize_subscribe_ex(MQTTStream& stream, unsigned char dup, unsigned short packetid, int count, MQTTString topicFilters[], int requestedQoSs[])
{
	stream.clear();
	int len = MQTTPacket_len(MQTTSerialize_subscribeLengthEx(count, topicFilters));
	stream.capacity(len);
	return MQTTSerialize_subscribe(stream.begin(), len, dup, packetid, count, topicFilters, requestedQoSs);
}

int MQTTSerialize_unsubscribe_ex(MQTTStream& stream, unsigned char dup, unsigned short packetid, int count, MQTTString topicFilters[])
{
	stream.clear();
	int len = MQTTPacket_len(MQTTSerialize_unsubscribeLengthEx(count, topicFilters));
	stream.capacity(len);
	return MQTTSerialize_unsubscribe(stream.begin(), len, dup, packetid, count, topicFilters);
}

int MQTTSerialize_publish_ex(MQTTStream& stream, unsigned char dup, int qos, unsigned char retained, unsigned short packetid, MQTTString topicName, unsigned char* payload, int payloadlen)
{
	stream.clear();
	int len = MQTTPacket_len(MQTTSerialize_publishLengthEx(qos, topicName, payloadlen));
	stream.capacity(len);
	return MQTTSerialize_publish(stream.begin(), len, dup, qos, retained, packetid, topicName, payload, payloadlen);
}

int MQTTSerialize_disconnect_ex(MQTTStream& stream)
{	
	stream.clear();
	const int SIZE = 2;
	stream.capacity(SIZE);
	return MQTTSerialize_disconnect(stream.begin(), SIZE);
}