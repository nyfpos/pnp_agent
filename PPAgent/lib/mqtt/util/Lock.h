
#ifndef __SWLIB_LOCK_H
#define __SWLIB_LOCK_H

#include <Windows.h>
#include <string>
#include <vector>
#include <map>

#define SW_TOSTR(s) #s

namespace swlib
{
	//////////////////  CriticalSession  //////////////////////

	class CriticalSession
	{
	public:
		class AutoLock
		{
		private:
			CriticalSession&	m_lock;
		public:
			AutoLock(CriticalSession& lock):m_lock(lock) { m_lock.Lock(); }
			~AutoLock() {m_lock.Unlock(); }
		};
		friend class AutoLock;
	private:
		CRITICAL_SECTION m_cs;
	public:
		CriticalSession() { InitializeCriticalSection(&m_cs); }
		virtual ~CriticalSession() { Unlock(); DeleteCriticalSection(&m_cs); }
		void Lock() { EnterCriticalSection(&m_cs ); }
		void Unlock() { LeaveCriticalSection(&m_cs); }
	};
	#define THREAD_LIB_LOCK_CRITICAL_SESSION(f) swlib::CriticalSession::AutoLock _macro_create_critical_session_lock##f(f);


	/////////////////// Mutex /////////////////////////////

	class Mutex
	{
	public:
		class AutoLock
		{
		private:
			Mutex&	m_mx;
		public:
			AutoLock(Mutex& mx):m_mx(mx) { m_mx.Lock(); }
			~AutoLock() {m_mx.Unlock(); }
		};
		friend class AutoLock;

	private:
		HANDLE	m_lock;
	public:
		Mutex() { m_lock = CreateMutex(0, FALSE, NULL); }
		~Mutex() {
			ReleaseMutex(m_lock); 
			CloseHandle(m_lock);
		}

		void Lock() { WaitForSingleObject(m_lock, INFINITE); }
		void Unlock() { ReleaseMutex( m_lock ); }
	};

	#define THREAD_LIB_LOCK_MUTEX(f) swlib::Mutex::AutoLock _macro_create_lock##f(f);	
}

#endif