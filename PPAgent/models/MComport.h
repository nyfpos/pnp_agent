#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\ComportData.h"
#include "query\QComport.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{
	template <typename Query>
	class MComport;

	template<>
	class MComport<QComport> : public IModel<ComportData, QComport>
	{
		typedef boost::shared_ptr<ComportData> ComportDataPtr;
	private:
		soci::session _sql;

	public:
		MComport(QComport* query) :IModel<ComportData, QComport>(query) 
		{ 
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MComport() { }
		bool Get(ComportData& data)
		{
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			int ComportNbr = _query->ComportNbr;

			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT top 1 ProjIdbw,ProjNodeIdbw,ComportIdbw,InterfaceName,ComportNbr,Description,Available,BaudRate,DataBit,StopBit,Parity,\
													   ScanTime,TimeOut,RetryCount,AutoRecoverTime,HandShakeRts,HandShakeDtr,RtsDelayAfterOn,RtsDalayBeforeOff,OPCServer,OPCServerType,PortData1,PortData2,PortData3,PortData4,\
													   PortText1,PortText2,Expand FROM pComport WHERE ProjNodeIdbw=:1 AND ComportNbr=:2", soci::use(ProjNodeIdbw), soci::use(ComportNbr), soci::into(row));
				st.execute();
				while (st.fetch())
				{
					int index = 0;					
					GetValidRowData(int, index++, row, data.ProjIdbw);
					GetValidRowData(int, index++, row, data.ProjNodeIdbw);
					GetValidRowData(int, index++, row, data.ComportIdbw);
					GetValidRowData(std::string, index++, row, data.InterfaceName);
					GetValidRowData(int, index++, row, data.ComportNbr);
					GetValidRowData(std::string, index++, row, data.Description);
					GetValidRowData(std::string, index++, row, data.Available);
					GetValidRowData(int, index++, row, data.BaudRate);
					GetValidRowData(int, index++, row, data.DataBit);
					GetValidRowData(int, index++, row, data.StopBit);
					GetValidRowData(int, index++, row, data.Parity);
					GetValidRowData(int, index++, row, data.ScanTime);
					GetValidRowData(int, index++, row, data.TimeOut);
					GetValidRowData(int, index++, row, data.RetryCount);
					GetValidRowData(int, index++, row, data.AutoRecoverTime);
					GetValidRowData(std::string, index++, row, data.HandShakeRts);
					GetValidRowData(std::string, index++, row, data.HandShakeDtr);
					GetValidRowData(int, index++, row, data.RtsDelayAfterOn);
					GetValidRowData(int, index++, row, data.RtsDalayBeforeOff);
					GetValidRowData(std::string, index++, row, data.OPCServer);
					GetValidRowData(std::string, index++, row, data.OPCServerType);
					GetValidRowData(int, index++, row, data.PortData1);
					GetValidRowData(int, index++, row, data.PortData2);
					GetValidRowData(int, index++, row, data.PortData3);
					GetValidRowData(int, index++, row, data.PortData4);
					GetValidRowData(std::string, index++, row, data.PortText1);
					GetValidRowData(std::string, index++, row, data.PortText2);
					GetValidRowData(std::string, index++, row, data.Expand);
					break;
				}				
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}			
		}
		bool Get(std::vector<ComportDataPtr>&vdata) 
		{ 
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT ProjIdbw,ProjNodeIdbw,ComportIdbw,InterfaceName,ComportNbr,Description,Available,BaudRate,DataBit,StopBit,Parity,\
													   ScanTime,TimeOut,RetryCount,AutoRecoverTime,HandShakeRts,HandShakeDtr,RtsDelayAfterOn,RtsDalayBeforeOff,OPCServer,OPCServerType,PortData1,PortData2,PortData3,PortData4,\
													   PortText1,PortText2,Expand FROM pComport WHERE ProjNodeIdbw=:1", soci::use(ProjNodeIdbw), soci::into(row));
				st.execute();
				while (st.fetch())
				{
					int index = 0;					
					ComportDataPtr data(new ComportData());
					GetValidRowData(int, index++, row, data->ProjIdbw);
					GetValidRowData(int, index++, row, data->ProjNodeIdbw);
					GetValidRowData(int, index++, row, data->ComportIdbw);
					GetValidRowData(std::string, index++, row, data->InterfaceName);
					GetValidRowData(int, index++, row, data->ComportNbr);
					GetValidRowData(std::string, index++, row, data->Description);
					GetValidRowData(std::string, index++, row, data->Available);
					GetValidRowData(int, index++, row, data->BaudRate);
					GetValidRowData(int, index++, row, data->DataBit);
					GetValidRowData(int, index++, row, data->StopBit);
					GetValidRowData(int, index++, row, data->Parity);
					GetValidRowData(int, index++, row, data->ScanTime);
					GetValidRowData(int, index++, row, data->TimeOut);
					GetValidRowData(int, index++, row, data->RetryCount);
					GetValidRowData(int, index++, row, data->AutoRecoverTime);
					GetValidRowData(std::string, index++, row, data->HandShakeRts);
					GetValidRowData(std::string, index++, row, data->HandShakeDtr);
					GetValidRowData(int, index++, row, data->RtsDelayAfterOn);
					GetValidRowData(int, index++, row, data->RtsDalayBeforeOff);
					GetValidRowData(std::string, index++, row, data->OPCServer);
					GetValidRowData(std::string, index++, row, data->OPCServerType);
					GetValidRowData(int, index++, row, data->PortData1);
					GetValidRowData(int, index++, row, data->PortData2);
					GetValidRowData(int, index++, row, data->PortData3);
					GetValidRowData(int, index++, row, data->PortData4);
					GetValidRowData(std::string, index++, row, data->PortText1);
					GetValidRowData(std::string, index++, row, data->PortText2);
					GetValidRowData(std::string, index++, row, data->Expand);
					vdata.push_back(data);
				}
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool GetUsedComport(std::vector<int>&comportNbrs)
		{
			int ProjNodeIdbw = _query->ProjNodeIdbw;

			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT ComportNbr FROM pComport WHERE ProjNodeIdbw=:1", soci::use(ProjNodeIdbw), soci::into(row));
				st.execute();
				while (st.fetch())
				{
					int index = 0, comportNbr = 0;
					GetValidRowData(int, index, row, comportNbr);
					if (comportNbr > 0)
						comportNbrs.push_back(comportNbr);
				}
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool Update(ComportData& data) { return true; }
		bool Update(std::vector<ComportDataPtr>&data) { return true; }
		bool Insert(ComportData& data)
		{
			try
			{
				    _sql << "INSERT INTO pComport(ProjIdbw,ProjNodeIdbw,InterfaceName,ComportNbr,Description,Available,BaudRate,\
					DataBit, StopBit, Parity, ScanTime, TimeOut, RetryCount, AutoRecoverTime, HandShakeRts, HandShakeDtr, RtsDelayAfterOn,\
					RtsDalayBeforeOff, OPCServer, OPCServerType, PortData1, PortData2, PortData3, PortData4, PortText1, PortText2,\
					expand) VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27)",
					soci::use(data.ProjIdbw), soci::use(data.ProjNodeIdbw), soci::use(data.InterfaceName), soci::use(data.ComportNbr), soci::use(data.Description), soci::use((std::string)data.Available), soci::use(data.BaudRate),
					soci::use(data.DataBit), soci::use(data.StopBit), soci::use(data.Parity), soci::use(data.ScanTime), soci::use(data.TimeOut), soci::use(data.RetryCount), soci::use(data.AutoRecoverTime), soci::use((std::string)data.HandShakeRts), soci::use((std::string)data.HandShakeDtr), soci::use(data.RtsDelayAfterOn),
					soci::use(data.RtsDalayBeforeOff), soci::use(data.OPCServer), soci::use(data.OPCServerType), soci::use(data.PortData1), soci::use(data.PortData2), soci::use(data.PortData3), soci::use(data.PortData4), 
					soci::use(data.PortText1), soci::use(data.PortText2), soci::use((std::string)data.Expand);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool Insert(std::vector<ComportDataPtr>&data) { return true; }
		bool Delete() 
		{ 
			std::vector<int>& comportNbrs = _query->ComportNbrs;
			int comportNbr = _query->ComportNbr;
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			try
			{
				if (comportNbrs.size() > 0)
				{
					std::string strComportNbrs = util::vectorToString<int>(",", comportNbrs);

					_sql << "DELETE FROM pComport WHERE ProjNodeIdbw=" << ProjNodeIdbw << " AND ComportNbr IN (" << strComportNbrs << ")";
					return true;
				}
				else if (comportNbr != -1) {
					_sql << "DELETE FROM pComport WHERE ProjNodeIdbw=:1 AND ComportNbr=:2", soci::use(ProjNodeIdbw), soci::use(comportNbr);
					return true;
				}
				else
					return false;
			}		
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
	};
}