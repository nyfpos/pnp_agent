#pragma once

#include <iostream>
#include "boost/shared_ptr.hpp"

namespace wa
{
	template <typename MData, typename Query>
	class IModel
	{
	protected:
		Query* _query;
	public:
		IModel(Query* query):_query(query) {}
		virtual ~IModel() = 0 {}
		virtual bool Get(MData& data) = 0;
		virtual bool Get(std::vector<boost::shared_ptr<MData> >&data) = 0;
		virtual bool Update(MData& data) = 0;
		virtual bool Update(std::vector<boost::shared_ptr<MData> >&data) = 0;
		virtual bool Insert(MData& data) = 0;
		virtual bool Insert(std::vector<boost::shared_ptr<MData> >&data) = 0;
		virtual bool Delete() = 0;
	};
}