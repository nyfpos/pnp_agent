#pragma once
#include <iostream>
namespace wa
{
	struct QComport
	{
		int ProjNodeIdbw;
		int ComportNbr;
		std::vector<int> ComportNbrs;
		QComport()
		{
			ProjNodeIdbw = 0;
			ComportNbr = -1;
		}
	};
}