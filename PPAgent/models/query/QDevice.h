#pragma once
#include <iostream>
namespace wa
{
	struct QDevice
	{
		int ProjIdbw;
		int ProjNodeIdbw;		
		int ComportIdbw;
		int DeviceIdbw;
		std::string DeviceName;
		std::string ProjName;
		std::string NodeName;
		QDevice()
		{
			ProjIdbw = -1;
			ProjNodeIdbw = -1;
			ComportIdbw = -1;
			DeviceIdbw = -1;
		}
	};
}