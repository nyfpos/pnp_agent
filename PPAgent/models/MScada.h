#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\ScadaData.h"
#include "query\QScada.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{
	template <typename Query>
	class MScada;

	template<>
	class MScada<QScada> : public IModel<ScadaData, QScada>
	{
		typedef boost::shared_ptr<ScadaData> ScadaDataPtr;
	private:
		soci::session _sql;

	public:
		MScada(QScada* query) : IModel<ScadaData, QScada>(query)
		{
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MScada() { }
		bool Get(ScadaData& data)
		{			
			int ProjIdbw = _query->ProjIdbw;
			std::string NodeName = std::string(_query->NodeName);
			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT top 1 ProjIdbw,ProjNodeIdbw,NodeName,NodeDesc,Address,PortNbr1,PortNbr2,NodeTimeOut,AccessSecurityCode,ReservedText1,ReservedText2,ReservedInt1,ReservedInt2,expand,expandAcc,expandCalc,expandConst,IPList FROM pNode WHERE ProjIdbw=:id AND NodeName=:name", soci::use(ProjIdbw), soci::use(NodeName), soci::into(row));
				st.execute();
				while (st.fetch())
				{
					int index = 0;					
					GetValidRowData(int, index++, row, data.ProjIdbw);
					GetValidRowData(int, index++, row, data.ProjNodeIdbw);
					GetValidRowData(std::string, index++, row, data.NodeName);
					GetValidRowData(std::string, index++, row, data.NodeDesc);
					GetValidRowData(std::string, index++, row, data.Address);
					GetValidRowData(int, index++, row, data.PortNbr1);
					GetValidRowData(int, index++, row, data.PortNbr2);
					GetValidRowData(int, index++, row, data.NodeTimeOut);
					GetValidRowData(std::string, index++, row, data.AccessSecurityCode);
					GetValidRowData(std::string, index++, row, data.ReservedText1);
					GetValidRowData(std::string, index++, row, data.ReservedText2);
					GetValidRowData(int, index++, row, data.ReservedInt1);
					GetValidRowData(int, index++, row, data.ReservedInt2);
					GetValidRowData(std::string, index++, row, data.expand);
					GetValidRowData(std::string, index++, row, data.expandAcc);
					GetValidRowData(std::string, index++, row, data.expandCalc);
					GetValidRowData(std::string, index++, row, data.IPList);
					GetValidRowData(std::string, index++, row, data.IPList);
					//printf("%s %s %s %s\n", data.expand ? "true" : "false", data.expandAcc ? "true" : "false", data.expandCalc ? "true" : "false", data.expandConst ? "true" : "false");
					break;
				}
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}			
		}
		bool Get(std::vector<ScadaDataPtr>&data) { return true; }
		bool Update(ScadaData& data) { return true; }
		bool Update(std::vector<ScadaDataPtr>&data) { return true; }
		bool Insert(ScadaData& data)
		{
			try
			{
				std::string _true("1");
				std::string _false("0");
				_sql << "INSERT INTO pNode(ProjIdbw,NodeName,NodeDesc,Address,PortNbr1,PortNbr2,NodeTimeOut,AccessSecurityCode,ReservedText1,ReservedText2,ReservedInt1,ReservedInt2,expand,expandAcc,expandCalc,expandConst,IPList) VALUES\
							  (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)", 
					soci::use(data.ProjIdbw), soci::use(data.NodeName), soci::use(data.NodeDesc), soci::use(data.Address), soci::use(data.PortNbr1), soci::use(data.PortNbr2), soci::use(data.NodeTimeOut), soci::use(data.AccessSecurityCode), soci::use(data.ReservedText1), 
					soci::use(data.ReservedText2), soci::use(data.ReservedInt1), soci::use(data.ReservedInt2), soci::use(data.expand ? _true : _false), soci::use(data.expandAcc ? _true : _false), soci::use(data.expandCalc ? _true : _false), soci::use(data.expandConst ? _true : _false), soci::use(data.IPList);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool Insert(std::vector<ScadaDataPtr>&data) { return true; }
		bool Delete() { return true; }
	};
}