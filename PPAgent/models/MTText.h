#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\TTextData.h"

#include "query\QTag.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{
	template <typename Query>
	class MTText;

	template<>
	class MTText<QTag> : public IModel<TTextData, QTag>
	{
		typedef boost::shared_ptr<TTextData> TTextDataPtr;
	private:
		soci::session _sql;

	public:
		MTText(QTag* query) :IModel<TTextData, QTag>(query)
		{
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MTText() { }

		bool Get(TTextData& data)
		{
			return true;
		}

		bool Get(std::vector<TTextDataPtr>&data)
		{
			return true;
		}

		bool Update(TTextData& data)
		{
			return true;
		}

		bool Update(std::vector<TTextDataPtr>&data) { return true; }

		bool Insert(TTextData& data)
		{
			return true;
		}

		bool Insert(std::vector<TTextDataPtr>&data){ return true; }

		bool Delete()
		{
			return true;
		}
	};
}