#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\TAnalogData.h"

#include "query\QTag.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{
	template <typename Query>
	class MTAnalog;

	template<>
	class MTAnalog<QTag> : public IModel<TAnalogData, QTag>
	{
		typedef boost::shared_ptr<TAnalogData> TAnalogDataPtr;
	private:
		soci::session _sql;

	public:
		MTAnalog(QTag* query) :IModel<TAnalogData, QTag>(query)
		{
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MTAnalog() { }

		bool Get(TAnalogData& data)
		{
			return true;
		}

		bool Get(std::vector<TAnalogDataPtr>&vdata)
		{
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			int DeviceIdbw = _query->DeviceIdbw;
			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT ProjNodeIdbw, TagName, Description, ScanType, ParaName, DeviceIdbw, Address, ConversionCode, \
													  		  StartBit, Length, SignalReverse, ScalingType, ScalingFactor1, ScalingFactor2, Datalog, DataLogDB, \
															  ChangeLog, ReadOnly, KeepPrevVal, Value, SecurityArea, SecurityLevel, SpanHigh, SpanLow, OutputHigh, \
															  OutputLow, EngUnit, IntDspFmt, FraDspFmt, ReservedInt1, ReservedInt2, ReservedText1, ReservedText2, ArrayItemNumber, \
															  UserDefinedInt1, UserDefinedInt2, UserDefinedInt3, UserDefinedInt4, UserDefinedInt5, UserDefinedText1, UserDefinedText2, \
															  UserDefinedText3, UserDefinedText4, UserDefinedText5 FROM tAnalog WHERE ProjNodeIdbw=:1 and DeviceIdbw=:2", soci::use(ProjNodeIdbw), soci::use(DeviceIdbw), soci::into(row));
				st.execute();
				while (st.fetch())
				{
					int index = 0;
					TAnalogDataPtr data(new TAnalogData());
					GetValidRowData(int, index++, row, data->ProjNodeIdbw);
					GetValidRowData(std::string, index++, row, data->TagName);
					GetValidRowData(std::string, index++, row, data->Description);
					GetValidRowData(int, index++, row, data->ScanType);
					GetValidRowData(std::string, index++, row, data->ParaName);
					GetValidRowData(int, index++, row, data->DeviceIdbw);
					GetValidRowData(std::string, index++, row, data->Address);
					GetValidRowData(int, index++, row, data->ConversionCode);
					GetValidRowData(int, index++, row, data->StartBit);
					GetValidRowData(int, index++, row, data->Length);
					GetValidRowData(std::string, index++, row, data->SignalReverse);
					GetValidRowData(int, index++, row, data->ScalingType);
					GetValidRowData(double, index++, row, data->ScalingFactor1);
					GetValidRowData(double, index++, row, data->ScalingFactor2);
					GetValidRowData(std::string, index++, row, data->Datalog);
					GetValidRowData(double, index++, row, data->DataLogDB);
					GetValidRowData(std::string, index++, row, data->ChangeLog);
					GetValidRowData(std::string, index++, row, data->ReadOnly);
					GetValidRowData(std::string, index++, row, data->KeepPrevVal);
					GetValidRowData(double, index++, row, data->Value);
					GetValidRowData(int, index++, row, data->SecurityArea);
					GetValidRowData(int, index++, row, data->SecurityLevel);
					GetValidRowData(double, index++, row, data->SpanHigh);
					GetValidRowData(double, index++, row, data->SpanLow);
					GetValidRowData(double, index++, row, data->OutputHigh);
					GetValidRowData(double, index++, row, data->OutputLow);
					GetValidRowData(std::string, index++, row, data->EngUnit);
					GetValidRowData(int, index++, row, data->IntDspFmt);
					GetValidRowData(int, index++, row, data->FraDspFmt);
					GetValidRowData(int, index++, row, data->ReservedInt1);
					GetValidRowData(int, index++, row, data->ReservedInt2);
					GetValidRowData(std::string, index++, row, data->ReservedText1);
					GetValidRowData(std::string, index++, row, data->ReservedText2);
					GetValidRowData(int, index++, row, data->ArrayItemNumber);
					GetValidRowData(double, index++, row, data->UserDefinedInt1);
					GetValidRowData(double, index++, row, data->UserDefinedInt2);
					GetValidRowData(double, index++, row, data->UserDefinedInt3);
					GetValidRowData(double, index++, row, data->UserDefinedInt4);
					GetValidRowData(double, index++, row, data->UserDefinedInt5);
					GetValidRowData(std::string, index++, row, data->UserDefinedText1);
					GetValidRowData(std::string, index++, row, data->UserDefinedText2);
					GetValidRowData(std::string, index++, row, data->UserDefinedText3);
					GetValidRowData(std::string, index++, row, data->UserDefinedText4);
					GetValidRowData(std::string, index++, row, data->UserDefinedText5);
					vdata.push_back(data);
				}
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}

			return true;
		}

		bool Update(TAnalogData& data)
		{
			return true;
		}

		bool Update(std::vector<TAnalogDataPtr>&data) { return true; }

		bool Insert(TAnalogData& data)
		{
			try
			{
			_sql << "INSERT INTO tAnalog(ProjNodeIdbw,TagName,Description,ScanType,ParaName,DeviceIdbw,Address,ConversionCode, \
			             StartBit, [Length], SignalReverse, ScalingType, ScalingFactor1, ScalingFactor2, Datalog, DataLogDB, ChangeLog, \
						 ReadOnly, KeepPrevVal, [Value], SecurityArea, SecurityLevel, SpanHigh, SpanLow, OutputHigh, OutputLow, EngUnit, \
						 IntDspFmt, FraDspFmt, ReservedInt1, ReservedInt2, ReservedText1, ReservedText2, ArrayItemNumber, UserDefinedInt1, \
						 UserDefinedInt2, UserDefinedInt3, UserDefinedInt4, UserDefinedInt5, UserDefinedText1, UserDefinedText2, UserDefinedText3, \
						 UserDefinedText4, UserDefinedText5) VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39, :40, :41, :42, :43, :44)",
						 soci::use(data.ProjNodeIdbw),
						 soci::use(data.TagName),
						 soci::use(data.Description),
						 soci::use(data.ScanType),
						 soci::use(data.ParaName),
						 soci::use(data.DeviceIdbw),
						 soci::use(data.Address),
						 soci::use(data.ConversionCode),
						 soci::use(data.StartBit),
						 soci::use(data.Length),
						 soci::use((std::string)data.SignalReverse),
						 soci::use(data.ScalingType),
						 soci::use(data.ScalingFactor1),
						 soci::use(data.ScalingFactor2),
						 soci::use((std::string)data.Datalog),
						 soci::use(data.DataLogDB),
						 soci::use((std::string)data.ChangeLog),
						 soci::use((std::string)data.ReadOnly),
						 soci::use((std::string)data.KeepPrevVal),
						 soci::use(data.Value),
						 soci::use(data.SecurityArea),
						 soci::use(data.SecurityLevel),
						 soci::use(data.SpanHigh),
						 soci::use(data.SpanLow),
						 soci::use(data.OutputHigh),
						 soci::use(data.OutputLow),
						 soci::use(data.EngUnit),
						 soci::use(data.IntDspFmt),
						 soci::use(data.FraDspFmt),
						 soci::use(data.ReservedInt1),
						 soci::use(data.ReservedInt2),
						 soci::use(data.ReservedText1),
						 soci::use(data.ReservedText2),
						 soci::use(data.ArrayItemNumber),
						 soci::use(data.UserDefinedInt1),
						 soci::use(data.UserDefinedInt2),
						 soci::use(data.UserDefinedInt3),
						 soci::use(data.UserDefinedInt4),
						 soci::use(data.UserDefinedInt5),
						 soci::use(data.UserDefinedText1),
						 soci::use(data.UserDefinedText2),
						 soci::use(data.UserDefinedText3),
						 soci::use(data.UserDefinedText4),
						 soci::use(data.UserDefinedText5);

				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}

		bool Insert(std::vector<TAnalogDataPtr>&data){ return true; }

		bool Delete()
		{
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			int DeviceIdbw = _query->DeviceIdbw;
			try
			{
				_sql << "DELETE FROM tAnalog WHERE ProjNodeIdbw=:1 AND DeviceIdbw=:2", soci::use(ProjNodeIdbw), soci::use(DeviceIdbw);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
	};
}