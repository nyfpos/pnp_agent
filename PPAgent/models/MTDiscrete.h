#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\TDiscreteData.h"

#include "query\QTag.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{
	template <typename Query>
	class MTDiscrete;

	template<>
	class MTDiscrete<QTag> : public IModel<TDiscreteData, QTag>
	{
		typedef boost::shared_ptr<TDiscreteData> TDiscreteDataPtr;
	private:
		soci::session _sql;

	public:
		MTDiscrete(QTag* query) :IModel<TDiscreteData, QTag>(query)
		{
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MTDiscrete() { }

		bool Get(TDiscreteData& data)
		{
			return true;
		}

		bool Get(std::vector<TDiscreteDataPtr>&data)
		{
			return true;
		}

		bool Update(TDiscreteData& data)
		{
			return true;
		}

		bool Update(std::vector<TDiscreteDataPtr>&data) { return true; }

		bool Insert(TDiscreteData& data)
		{
			return true;
		}

		bool Insert(std::vector<TDiscreteDataPtr>&data){ return true; }

		bool Delete()
		{
			return true;
		}
	};
}