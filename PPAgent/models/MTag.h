#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\TagData.h"
#include "query\QTag.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{
	template <typename Query>
	class MTag;

	template<>
	class MTag<QTag> : public IModel<TagData, QTag>
	{
		typedef boost::shared_ptr<TagData> TagDataPtr;
	private:
		soci::session _sql;

	public:
		MTag(QTag* query) :IModel<TagData, QTag>(query)
		{
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MTag() { }

		bool Get(TagData& data)
		{
			return true;
		}

		bool Get(std::vector<TagDataPtr>&data)
		{
			return true;
		}

		bool Update(TagData& data)
		{
			return true;
		}

		bool Update(std::vector<TagDataPtr>&data) { return true; }

		bool Insert(TagData& data)
		{
			try
			{
				_sql << "INSERT INTO pTag(ProjIdbw,ProjNodeIdbw,ComportIdbw,DeviceIdbw,TagName,TagType) VALUES (:1,:2,:3,:4,:5,:6)", 
					    soci::use(data.ProjIdbw), soci::use(data.ProjNodeIdbw), soci::use(data.ComportIdbw),
						soci::use(data.DeviceIdbw), soci::use(data.TagName), soci::use(data.TagType);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}

		bool Insert(std::vector<TagDataPtr>&data){ return true; }

		bool Delete()
		{
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			int DeviceIdbw = _query->DeviceIdbw;
			try
			{
				_sql << "DELETE FROM pTag WHERE ProjNodeIdbw=:1 AND DeviceIdbw=:2", soci::use(ProjNodeIdbw), soci::use(DeviceIdbw);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}

	};
}