#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\DeviceData.h"
#include "query\QDevice.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{
	template <typename Query>
	class MDevice;

	template<>
	class MDevice<QDevice> : public IModel <DeviceData, QDevice>
	{
		typedef boost::shared_ptr<DeviceData> DeviceDataPtr;
	private:
		soci::session _sql;

	public:
		MDevice(QDevice* query) :IModel<DeviceData, QDevice>(query)
		{
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MDevice() { }
		bool Get(DeviceData& data)
		{
			std::string DeviceName(_query->DeviceName);
			int ComportIdbw(_query->ComportIdbw);
			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT top 1 ProjIdbw,ProjNodeIdbw,ComportIdbw,DeviceIdbw,DeviceName,ComportNbr,Description,UnitNumber,DeviceType,SubType,\
													   DeviceData1,DeviceData2,DeviceData3,DeviceData4,DeviceText1,DeviceText2,Expand FROM pDevice WHERE ComportIdbw=:1 AND DeviceName=:2", 
													   soci::use(ComportIdbw), soci::use(DeviceName), soci::into(row));
				st.execute();
				while (st.fetch())
				{
					int index = 0;					
					GetValidRowData(int, index++, row, data.ProjIdbw);
					GetValidRowData(int, index++, row, data.ProjNodeIdbw);
					GetValidRowData(int, index++, row, data.ComportIdbw);
					GetValidRowData(int, index++, row, data.DeviceIdbw);
					GetValidRowData(std::string, index++, row, data.DeviceName);
					GetValidRowData(int, index++, row, data.ComportNbr);
					GetValidRowData(std::string, index++, row, data.Description);
					GetValidRowData(int, index++, row, data.UnitNumber);
					GetValidRowData(std::string, index++, row, data.DeviceType);
					GetValidRowData(int, index++, row, data.SubType);
					GetValidRowData(int, index++, row, data.DeviceData1);
					GetValidRowData(int, index++, row, data.DeviceData2);
					GetValidRowData(int, index++, row, data.DeviceData3);
					GetValidRowData(int, index++, row, data.DeviceData4);
					GetValidRowData(std::string, index++, row, data.DeviceText1);
					GetValidRowData(std::string, index++, row, data.DeviceText2);
					GetValidRowData(std::string, index++, row, data.Expand);
					break;
				}
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}		
		}
		bool Get(std::vector<DeviceDataPtr>&vdata)
		{
			
			int ProjIdbw = _query->ProjIdbw;
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT ProjIdbw,ProjNodeIdbw,ComportIdbw,DeviceIdbw,DeviceName,ComportNbr,Description,UnitNumber,DeviceType,SubType,\
									                   DeviceData1,DeviceData2,DeviceData3,DeviceData4,DeviceText1,DeviceText2,Expand FROM pDevice WHERE ProjIdbw=:1 AND ProjNodeIdbw=:2",
									                   soci::use(ProjIdbw), soci::use(ProjNodeIdbw), soci::into(row));
				st.execute();
				while (st.fetch())
				{
					int index = 0;					
					DeviceDataPtr data(new DeviceData());
					GetValidRowData(int, index++, row, data->ProjIdbw);
					GetValidRowData(int, index++, row, data->ProjNodeIdbw);
					GetValidRowData(int, index++, row, data->ComportIdbw);
					GetValidRowData(int, index++, row, data->DeviceIdbw);
					GetValidRowData(std::string, index++, row, data->DeviceName);
					GetValidRowData(int, index++, row, data->ComportNbr);
					GetValidRowData(std::string, index++, row, data->Description);
					GetValidRowData(int, index++, row, data->UnitNumber);
					GetValidRowData(std::string, index++, row, data->DeviceType);
					GetValidRowData(int, index++, row, data->SubType);
					GetValidRowData(int, index++, row, data->DeviceData1);
					GetValidRowData(int, index++, row, data->DeviceData2);
					GetValidRowData(int, index++, row, data->DeviceData3);
					GetValidRowData(int, index++, row, data->DeviceData4);
					GetValidRowData(std::string, index++, row, data->DeviceText1);
					GetValidRowData(std::string, index++, row, data->DeviceText2);
					GetValidRowData(std::string, index++, row, data->Expand);
					vdata.push_back(data);
				}
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}			
		}
		bool Get(int ComportIdbw, std::vector<DeviceDataPtr>&vdata)
		{
			int ProjIdbw = _query->ProjIdbw;
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT ProjIdbw,ProjNodeIdbw,ComportIdbw,DeviceIdbw,DeviceName,ComportNbr,Description,UnitNumber,DeviceType,SubType,\
													   DeviceData1,DeviceData2,DeviceData3,DeviceData4,DeviceText1,DeviceText2,Expand FROM pDevice WHERE ProjIdbw=:1 AND ProjNodeIdbw=:2 AND ComportIdbw=:3",
												       soci::use(ProjIdbw), soci::use(ProjNodeIdbw), soci::use(ComportIdbw), soci::into(row) );
				st.execute();
				while (st.fetch())
				{
					int index = 0;					
					DeviceDataPtr data(new DeviceData());
					GetValidRowData(int, index++, row, data->ProjIdbw);
					GetValidRowData(int, index++, row, data->ProjNodeIdbw);
					GetValidRowData(int, index++, row, data->ComportIdbw);
					GetValidRowData(int, index++, row, data->DeviceIdbw);
					GetValidRowData(std::string, index++, row, data->DeviceName);
					GetValidRowData(int, index++, row, data->ComportNbr);
					GetValidRowData(std::string, index++, row, data->Description);
					GetValidRowData(int, index++, row, data->UnitNumber);
					GetValidRowData(std::string, index++, row, data->DeviceType);
					GetValidRowData(int, index++, row, data->SubType);
					GetValidRowData(int, index++, row, data->DeviceData1);
					GetValidRowData(int, index++, row, data->DeviceData2);
					GetValidRowData(int, index++, row, data->DeviceData3);
					GetValidRowData(int, index++, row, data->DeviceData4);
					GetValidRowData(std::string, index++, row, data->DeviceText1);
					GetValidRowData(std::string, index++, row, data->DeviceText2);
					GetValidRowData(std::string, index++, row, data->Expand);
					vdata.push_back(data);
				}
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}

		bool Get(std::string projName, std::string nodeName, std::vector<DeviceDataPtr>&vdata)
		{
			if (projName.length() == 0 || nodeName.length() == 0)
				return false;
			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT pDevice.ProjIdbw,pDevice.ProjNodeIdbw,pDevice.ComportIdbw,pDevice.DeviceIdbw,pDevice.DeviceName,pDevice.ComportNbr,pDevice.Description,pDevice.UnitNumber,\
                                                       pDevice.DeviceType, pDevice.SubType, pDevice.DeviceData1, pDevice.DeviceData2, pDevice.DeviceData3, pDevice.DeviceData4, pDevice.DeviceText1, pDevice.DeviceText2, pDevice.expand \
                                                       FROM pDevice INNER JOIN(pProject INNER JOIN pNode ON pProject.ProjIdbw = pNode.ProjIdbw) ON pDevice.ProjNodeIdbw=pNode.ProjNodeIdbw AND pDevice.ProjIdbw=pNode.ProjIdbw \
                                                       WHERE pProject.ProjName=:1 AND pNode.NodeName=:2", soci::use(projName), soci::use(nodeName), soci::into(row));
				st.execute();
				while (st.fetch())
				{
					int index = 0;					
					DeviceDataPtr data(new DeviceData());
					GetValidRowData(int, index++, row, data->ProjIdbw);
					GetValidRowData(int, index++, row, data->ProjNodeIdbw);
					GetValidRowData(int, index++, row, data->ComportIdbw);
					GetValidRowData(int, index++, row, data->DeviceIdbw);
					GetValidRowData(std::string, index++, row, data->DeviceName);
					GetValidRowData(int, index++, row, data->ComportNbr);
					GetValidRowData(std::string, index++, row, data->Description);
					GetValidRowData(int, index++, row, data->UnitNumber);
					GetValidRowData(std::string, index++, row, data->DeviceType);
					GetValidRowData(int, index++, row, data->SubType);
					GetValidRowData(int, index++, row, data->DeviceData1);
					GetValidRowData(int, index++, row, data->DeviceData2);
					GetValidRowData(int, index++, row, data->DeviceData3);
					GetValidRowData(int, index++, row, data->DeviceData4);
					GetValidRowData(std::string, index++, row, data->DeviceText1);
					GetValidRowData(std::string, index++, row, data->DeviceText2);
					GetValidRowData(std::string, index++, row, data->Expand);
					vdata.push_back(data);
				}
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool Update(DeviceData& data) { return true; }
		bool Update(std::vector<DeviceDataPtr>&data) { return true; }
		bool Insert(DeviceData& data)
		{
			try
			{
				_sql << "INSERT INTO pDevice(ProjIdbw,ProjNodeIdbw,ComportIdbw,DeviceName,ComportNbr,Description,UnitNumber,\
						 DeviceType, SubType, DeviceData1, DeviceData2, DeviceData3, DeviceData4, DeviceText1, DeviceText2, expand) VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16)",
						 soci::use(data.ProjIdbw), soci::use(data.ProjNodeIdbw), soci::use(data.ComportIdbw), soci::use(data.DeviceName), soci::use(data.ComportNbr),
						 soci::use(data.Description), soci::use(data.UnitNumber), soci::use(data.DeviceType), soci::use(data.SubType), soci::use(data.DeviceData1), 
						 soci::use(data.DeviceData2), soci::use(data.DeviceData3), soci::use(data.DeviceData4), soci::use(data.DeviceText1), soci::use(data.DeviceText2), soci::use((std::string)data.Expand);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool Insert(std::vector<DeviceDataPtr>&data) 
		{ 
			return true; 
		}
		bool Delete()
		{			
			int ProjNodeIdbw = _query->ProjNodeIdbw;
			int DeviceIdbw = _query->DeviceIdbw;
			try
			{
				_sql << "DELETE FROM pDevice WHERE ProjNodeIdbw=:1 AND DeviceIdbw=:2", soci::use(ProjNodeIdbw), soci::use(DeviceIdbw);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}			
		}
	};
}