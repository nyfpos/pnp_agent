#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\ProjectData.h"
#include "query\QProject.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{	
	template <typename Query>
	class MProject;
	
	template<>
	class MProject<QProject> : public IModel <ProjectData, QProject>
	{
		typedef boost::shared_ptr<ProjectData> ProjectDataPtr;

	private:
		soci::session _sql;

	public:
		MProject(QProject* query) : IModel<ProjectData, QProject>(query) 
		{
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MProject() { }

		bool Get(ProjectData& data)
		{
			std::string projName = std::string(_query->ProjName);
			try
			{
				soci::row row;
				soci::statement st = (_sql.prepare << "SELECT top 1 ProjIdbw,ProjName,ProjDesc,ProjPath,ProjIP,ProjPort,ProjTimeOut,AccessSecurityCode,ReservedText1,ReservedText2,ReservedInt1,ReservedInt2 FROM pProject WHERE ProjName=:name", soci::use(projName), soci::into(row));				
				st.execute();				
				while (st.fetch())
				{
					int index = 0;
					GetValidRowData(int, index++, row, data.ProjIdbw);
					GetValidRowData(std::string, index++, row, data.ProjName);
					GetValidRowData(std::string, index++, row, data.ProjDesc);
					GetValidRowData(std::string, index++, row, data.ProjPath);
					GetValidRowData(std::string, index++, row, data.ProjIP);
					GetValidRowData(int, index++, row, data.ProjPort);
					GetValidRowData(int, index++, row, data.ProjTimeOut);
					GetValidRowData(std::string, index++, row, data.AccessSecurityCode);
					GetValidRowData(std::string, index++, row, data.ReservedText1);
					GetValidRowData(std::string, index++, row, data.ReservedText2);
					GetValidRowData(int, index++, row, data.ReservedInt1);
					GetValidRowData(int, index++, row, data.ReservedInt2);
					break;
				}
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool Get(std::vector<ProjectDataPtr>&data) { return true; }
		bool Update(ProjectData& data) { return true; }
		bool Update(std::vector<ProjectDataPtr>&data) { return true; }
		bool Insert(ProjectData& data)
		{
			try
			{
				_sql << "INSERT INTO pProject(ProjName,ProjDesc,ProjPath,ProjIP,ProjPort,ProjTimeOut,AccessSecurityCode,ReservedText1,ReservedText2,ReservedInt1,ReservedInt2)\
			             VALUES(:name,:desc,:path,:ip,:port,:timeout,:acode,:rtxt1,:rtxt2,:rint1,:rint2)",
						 soci::use(data.ProjName), soci::use(data.ProjDesc), soci::use(data.ProjPath), soci::use(data.ProjIP), soci::use(data.ProjPort), soci::use(data.ProjTimeOut),
						 soci::use(data.AccessSecurityCode), soci::use(data.ReservedText1), soci::use(data.ReservedText2), soci::use(data.ReservedInt1), soci::use(data.ReservedInt2);
				return true;
			}
			catch (std::exception const & e) {
				std::cout <<  e.what() << std::endl;
				return false;
			}
		}
		bool Insert(std::vector<ProjectDataPtr>&data) { return true; }
		bool Delete() { return true; }
	};
}