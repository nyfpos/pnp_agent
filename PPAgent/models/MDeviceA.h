#pragma once

#include "..\pch.h"

#include "IModel.h"
#include "data\DeviceAData.h"
#include "query\QDevice.h"

#include "..\util\util.h"

#include "soci.h"
#include "soci-odbc.h"

namespace wa
{
	template <typename Query>
	class MDeviceA;

	template<>
	class MDeviceA<QDevice> : public IModel <DeviceAData, QDevice>
	{
		typedef boost::shared_ptr<DeviceAData> DeviceADataPtr;
	private:
		soci::session _sql;

	public:
		MDeviceA(QDevice* query) :IModel<DeviceAData, QDevice>(query)
		{
			_sql.open(soci::odbc, DB_CONNECT_STR);
		}
		~MDeviceA() { }
		bool Get(DeviceAData& data)
		{
			int DeviceIdbw(_query->DeviceIdbw);
			try
			{
				soci::rowset<soci::row> rs = (_sql.prepare << "SELECT top 1 ProjNodeIdbw,ComportIdbw,DeviceIdbw,ComportNbr,UnitNumber,DeviceData1,DeviceData2,DeviceData3,DeviceData4,DeviceText1,DeviceText2,DeviceText3,DeviceText4,\
															  DeviceText5,DeviceText6,DeviceText7,DeviceText8 * FROM pDeviceA WHERE DeviceIdbw=:1", soci::use(DeviceIdbw));
				for (soci::rowset<soci::row>::const_iterator it = rs.begin(); it != rs.end(); ++it)
				{
					int index = 0;
					soci::row const& row = *it;
					GetValidRowData(int, index++, row, data.ProjNodeIdbw);
					GetValidRowData(int, index++, row, data.ComportIdbw);
					GetValidRowData(int, index++, row, data.DeviceIdbw);
					GetValidRowData(int, index++, row, data.ComportNbr);
					GetValidRowData(int, index++, row, data.UnitNumber);

					GetValidRowData(int, index++, row, data.DeviceData1);
					GetValidRowData(int, index++, row, data.DeviceData2);
					GetValidRowData(int, index++, row, data.DeviceData3);
					GetValidRowData(int, index++, row, data.DeviceData4);
					GetValidRowData(std::string, index++, row, data.DeviceText1);
					GetValidRowData(std::string, index++, row, data.DeviceText2);
					GetValidRowData(std::string, index++, row, data.DeviceText3);
					GetValidRowData(std::string, index++, row, data.DeviceText4);
					GetValidRowData(std::string, index++, row, data.DeviceText5);
					GetValidRowData(std::string, index++, row, data.DeviceText6);
					GetValidRowData(std::string, index++, row, data.DeviceText7);
					GetValidRowData(std::string, index++, row, data.DeviceText8);
					break;
				}
				return true;
			}				
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool Get(std::vector<DeviceADataPtr>&vdata)
		{
			return true;
		}
		bool Update(DeviceAData& data) { return true; }
		bool Update(std::vector<DeviceADataPtr>&data) { return true; }
		bool Insert(DeviceAData& data)
		{
			try
			{
				_sql.once << "INSERT INTO pDeviceA(ProjNodeIdbw,ComportIdbw,DeviceIdbw,ComportNbr,UnitNumber,DeviceData1,DeviceData2,DeviceData3,DeviceData4,\
					          DeviceText1,DeviceText2,DeviceText3,DeviceText4,DeviceText5,DeviceText6,DeviceText7,DeviceText8) VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17)",
							 soci::use(data.ProjNodeIdbw), soci::use(data.ComportIdbw), soci::use(data.DeviceIdbw), soci::use(data.ComportNbr), soci::use(data.UnitNumber),
							 soci::use(data.DeviceData1), soci::use(data.DeviceData2), soci::use(data.DeviceData3), soci::use(data.DeviceData4), soci::use(data.DeviceText1),
							 soci::use(data.DeviceText2), soci::use(data.DeviceText3), soci::use(data.DeviceText4), soci::use(data.DeviceText5), soci::use(data.DeviceText6), soci::use(data.DeviceText7), soci::use(data.DeviceText8);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
		bool Insert(std::vector<DeviceADataPtr>&data)
		{
			return true;
		}
		bool Delete()
		{
			int DeviceIdbw = _query->DeviceIdbw;
			try
			{
				_sql.once << "DELETE FROM pDeviceA WHERE DeviceIdbw=:1", soci::use(DeviceIdbw);
				return true;
			}
			catch (std::exception const & e) {
				std::cout << e.what() << std::endl;
				return false;
			}
		}
	};
}