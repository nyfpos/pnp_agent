#pragma once
#include <iostream>
#include "..\util\convert.h"
namespace wa
{
	struct TTextData
	{
		int ProjNodeIdbw;
		std::string TagName;
		std::string Description;
		int ScanType;
		std::string ParaName;
		int DeviceIdbw;
		std::string Address;
		int ConversionCode;
		int TextLen;
		Boolean ChangeLog;
		Boolean ReadOnly;
		Boolean KeepPrevVal;
		std::string Value;
		int SecurityArea;
		int SecurityLevel;
		int ReservedInt1;
		int ReservedInt2;
		std::string ReservedText1;
		std::string ReservedText2;
		int ArrayItemNumber;
		double UserDefinedInt1;
		double UserDefinedInt2;
		double UserDefinedInt3;
		double UserDefinedInt4;
		double UserDefinedInt5;
		std::string UserDefinedText1;
		std::string UserDefinedText2;
		std::string UserDefinedText3;
		std::string UserDefinedText4;
		std::string UserDefinedText5;

		TTextData()
		{
			Description = "";
			ScanType = 1;
			Address = "";
			ConversionCode = 0;
			TextLen = 16;
			ChangeLog = true;
			ReadOnly = false;
			KeepPrevVal = false;
			Value = "";
			SecurityArea = 0;
			SecurityLevel = 0;
			ReservedInt1 = 0;
			ReservedInt2 = 0;
			ReservedText1 = "";
			ReservedText2 = "";
			ArrayItemNumber = 0;
			UserDefinedInt1 = 0;
			UserDefinedInt2 = 0;
			UserDefinedInt3 = 0;
			UserDefinedInt4 = 0;
			UserDefinedInt5 = 0;
			UserDefinedText1 = "";
			UserDefinedText2 = "";
			UserDefinedText3 = "";
			UserDefinedText4 = "";
			UserDefinedText5 = "";
		}
	};
}