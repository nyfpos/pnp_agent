#pragma once
#include <iostream>
#include "..\util\convert.h"
namespace wa
{
	struct TDiscreteData
	{
		int ProjNodeIdbw;
		std::string TagName;
		std::string Description;
		int ScanType;
		std::string ParaName;
		int DeviceIdbw;
		std::string Address;
		int ConversionCode;
		int StartBit;
		int Length;
		Boolean SignalReverse;
		Boolean Datalog;
		double DataLogDB;
		Boolean ChangeLog;
		Boolean ReadOnly;
		Boolean KeepPrevVal;
		int Value;
		int SecurityArea;
		int SecurityLevel;
		std::string State_0;
		std::string State_1;
		std::string State_2;
		std::string State_3;
		std::string State_4;
		std::string State_5;
		std::string State_6;
		std::string State_7;
		int ReservedInt1;
		int ReservedInt2;
		std::string ReservedText1;
		std::string ReservedText2;
		int ArrayItemNumber;
		double UserDefinedInt1;
		double UserDefinedInt2;
		double UserDefinedInt3;
		double UserDefinedInt4;
		double UserDefinedInt5;
		std::string UserDefinedText1;
		std::string UserDefinedText2;
		std::string UserDefinedText3;
		std::string UserDefinedText4;
		std::string UserDefinedText5;

		TDiscreteData()
		{
			Description = "";
			ScanType = 1;
			Address = "";
			ConversionCode = 0;
			StartBit = 0;
			Length = 1;
			SignalReverse = false;
			Datalog = false;
			DataLogDB = 0;
			ChangeLog = true;
			ReadOnly = false;
			KeepPrevVal = false;
			Value = 0;
			SecurityArea = 0;
			SecurityLevel = 0;
			State_0 = "";
			State_1 = "";
			State_2 = "";
			State_3 = "";
			State_4 = "";
			State_5 = "";
			State_6 = "";
			State_7 = "";
			ReservedInt1 = 0;
			ReservedInt2 = 0;
			ReservedText1 = "";
			ReservedText2 = "";
			ArrayItemNumber = 0;
			UserDefinedInt1 = 0;
			UserDefinedInt2 = 0;
			UserDefinedInt3 = 0;
			UserDefinedInt4 = 0;
			UserDefinedInt5 = 0;
			UserDefinedText1 = "";
			UserDefinedText2 = "";
			UserDefinedText3 = "";
			UserDefinedText4 = "";
			UserDefinedText5 = "";
		}
	};
}