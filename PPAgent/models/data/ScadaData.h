#pragma once
#include <iostream>
#include "..\util\convert.h"
namespace wa
{
	struct ScadaData
	{
		int ProjIdbw;
		int ProjNodeIdbw;
		std::string NodeName;
		std::string NodeDesc;
		std::string Address;
		int PortNbr1;
		int PortNbr2;
		int NodeTimeOut;
		std::string AccessSecurityCode;
		std::string ReservedText1;
		std::string ReservedText2;
		int ReservedInt1;
		int ReservedInt2;
		Boolean expand;
		Boolean expandAcc;
		Boolean expandCalc;
		Boolean expandConst;
		std::string IPList;

		ScadaData()
		{
			ProjIdbw = 0;
			ProjNodeIdbw = 0;
			PortNbr1 = 0;
			PortNbr2 = 0;
			NodeTimeOut = 0;
			ReservedInt1 = 0;
			ReservedInt2 = 0;
			expand = false;
			expandAcc = false;
			expandCalc = false;
			expandConst = false;
		}

	};
}