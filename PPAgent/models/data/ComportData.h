#pragma once
#include <iostream>
#include "..\util\convert.h"
namespace wa
{
	struct ComportData
	{
		int ProjIdbw;
		int ProjNodeIdbw;
		int ComportIdbw;
		std::string InterfaceName;
		int ComportNbr;
		std::string Description;
		Boolean Available;
		int BaudRate;
		int DataBit;
		int StopBit;
		int Parity;
		int ScanTime;
		int TimeOut;
		int RetryCount;
		int AutoRecoverTime;
		Boolean HandShakeRts;
		Boolean HandShakeDtr;
		int RtsDelayAfterOn;
		int RtsDalayBeforeOff;
		std::string OPCServer;
		std::string OPCServerType;
		int PortData1;
		int PortData2;
		int PortData3;
		int PortData4;
		std::string PortText1;
		std::string PortText2;
		Boolean Expand;

		ComportData()
		{
			ProjIdbw = 0;
			ProjNodeIdbw = 0;
			ComportIdbw = 0;
			ComportNbr = 0;
			Description = std::string("");
			Available = true;
			BaudRate = 9600;
			DataBit = 8;
			StopBit = 1;
			Parity = 0;
			ScanTime = 1000;
			TimeOut = 1000;
			RetryCount = 3;
			AutoRecoverTime = 60;
			HandShakeRts = true;
			HandShakeDtr = true;
			RtsDelayAfterOn = 0;
			RtsDalayBeforeOff = 0;
			OPCServer = std::string("");
			OPCServerType = std::string("Local");
			PortData1 = 0;
			PortData2 = 0;
			PortData3 = 0;
			PortData4 = 0;
			PortText1 = "";
			PortText2 = "";
			Expand = true;
		}
	};
}