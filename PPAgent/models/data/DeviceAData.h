#pragma once
#include <iostream>
#include "..\util\convert.h"
namespace wa
{
	struct DeviceAData
	{
		int ProjNodeIdbw;
		int ComportIdbw;
		int DeviceIdbw;
		int ComportNbr;
		int UnitNumber;
		int DeviceData1;
		int DeviceData2;
		int DeviceData3;
		int DeviceData4;
		std::string DeviceText1;
		std::string DeviceText2;
		std::string DeviceText3;
		std::string DeviceText4;
		std::string DeviceText5;
		std::string DeviceText6;
		std::string DeviceText7;
		std::string DeviceText8;

		DeviceAData()
		{
			ProjNodeIdbw =
			ComportIdbw =
			DeviceIdbw =
			ComportNbr =
			UnitNumber = 0;

			DeviceData1 = 0;
			DeviceData2 = 0;
			DeviceData3 = 0;
			DeviceData4 = 0;
			DeviceText1 = "";
			DeviceText2 = "";
			DeviceText3 = "";
			DeviceText4 = "";
			DeviceText5 = "";
			DeviceText6 = "";
			DeviceText7 = "";
			DeviceText8 = "";
		}
	};
}