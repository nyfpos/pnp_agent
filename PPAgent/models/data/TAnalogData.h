#pragma once
#include <iostream>
#include "..\util\convert.h"
namespace wa
{
	struct TAnalogData
	{
		int ProjNodeIdbw;
		std::string TagName;
		std::string Description;
		int ScanType;
		std::string ParaName;
		int DeviceIdbw;
		std::string Address;
		int ConversionCode;
		int StartBit;
		int Length;
		Boolean SignalReverse;
		int ScalingType;
		double ScalingFactor1;
		double ScalingFactor2;
		Boolean Datalog;
		double DataLogDB;
		Boolean ChangeLog;
		Boolean ReadOnly;
		Boolean KeepPrevVal;
		double Value;
		int SecurityArea;
		int SecurityLevel;
		double SpanHigh;
		double SpanLow;
		double OutputHigh;
		double OutputLow;
		std::string EngUnit;
		int IntDspFmt;
		int FraDspFmt;
		int ReservedInt1;
		int ReservedInt2;
		std::string ReservedText1;
		std::string ReservedText2;
		int ArrayItemNumber;
		double UserDefinedInt1;
		double UserDefinedInt2;
		double UserDefinedInt3;
		double UserDefinedInt4;
		double UserDefinedInt5;
		std::string UserDefinedText1;
		std::string UserDefinedText2;
		std::string UserDefinedText3;
		std::string UserDefinedText4;
		std::string UserDefinedText5;

		TAnalogData()
		{
			Description = "";
			ScanType = 1;
			Address = "";
			ConversionCode = 0;
			StartBit = 0;
			Length = 16;
			SignalReverse = false;
			ScalingType = 0;
			ScalingFactor1 = 1;
			ScalingFactor2 = 0;
			Datalog = false;
			DataLogDB = 0;
			ChangeLog = true;
			ReadOnly = false;
			KeepPrevVal = false;
			Value = 0;
			SecurityArea = 0;
			SecurityLevel = 0;
			SpanHigh = 100;
			SpanLow = 0;
			OutputHigh = 100;
			OutputLow = 0;
			EngUnit = "";
			IntDspFmt = 4;
			FraDspFmt = 2;
			ReservedInt1 = 0;
			ReservedInt2 = 0;
			ReservedText1 = "";
			ReservedText2 = "";
			ArrayItemNumber = 0;
			UserDefinedInt1 = 0;
			UserDefinedInt2 = 0;
			UserDefinedInt3 = 0;
			UserDefinedInt4 = 0;
			UserDefinedInt5 = 0;
			UserDefinedText1 = "";
			UserDefinedText2 = "";
			UserDefinedText3 = "";
			UserDefinedText4 = "";
			UserDefinedText5 = "";
		}
	};
}