#pragma once
#include <iostream>
namespace wa
{
	struct ProjectData
	{
		int ProjIdbw;
		std::string ProjName;
		std::string ProjDesc;
		std::string ProjPath;
		std::string ProjIP;
		int ProjPort;
		int ProjTimeOut;
		std::string AccessSecurityCode;
		std::string ReservedText1;
		std::string ReservedText2;
		int ReservedInt1;
		int ReservedInt2;

		ProjectData()
		{
			ProjIdbw = 0;
			ProjPort = 0;
			ReservedInt1 = 0;
			ReservedInt2 = 0;
		}

	};
}