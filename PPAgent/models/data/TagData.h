#pragma once
#include <iostream>
#include "..\util\convert.h"
namespace wa
{
	struct TagData
	{
		int ProjIdbw;
		int ProjNodeIdbw;
		int ComportIdbw;
		int DeviceIdbw;
		int TagIdbw;
		std::string TagName;
		std::string TagType;
	};
}