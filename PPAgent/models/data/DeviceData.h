#pragma once
#include <iostream>
#include "..\util\convert.h"
namespace wa
{
	struct DeviceData
	{
		int ProjIdbw;
		int ProjNodeIdbw;
		int ComportIdbw;
		int DeviceIdbw;
		std::string DeviceName;
		int ComportNbr;
		std::string Description;
		int UnitNumber;
		std::string DeviceType;
		int SubType;
		int DeviceData1;
		int DeviceData2;
		int DeviceData3;
		int DeviceData4;
		std::string DeviceText1;
		std::string DeviceText2;
		Boolean Expand;

		DeviceData()
		{
			ProjIdbw = 0;
			ProjNodeIdbw = 0;
			ComportIdbw = 0;
			DeviceIdbw = 0;
			ComportNbr = 0;

			Description = "";
			UnitNumber = 0;
			DeviceType = "";
			SubType = 0;
			DeviceData1 = 0;
			DeviceData2 = 0;
			DeviceData3 = 0;
			DeviceData4 = 0;
			DeviceText1 = "";
			DeviceText2 = "";
			Expand = true;
		}
	};
}