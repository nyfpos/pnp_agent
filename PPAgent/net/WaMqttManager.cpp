#include "WaMqttManager.h"

namespace wa
{
	MqttManager::MqttManager() : _curState(None), _timerEvtID(NONE_VALUE), _config(NULL), _ipstack(), _mqttClient(_ipstack)
	{

	}

	MqttManager::~MqttManager()
	{			
		if (_config)
		{
			delete _config;
			_config = NULL;
		}
		stop();						
	}

	int MqttManager::_connectIPStack()
	{
		const char* host = _config->host;
		int port = _config->port;
		const char* path = _config->path;
		int ssl = _config->ssl;
		int ietf_version = -1;
//			printf("%s %d %d %s %s %s", host, port, ssl, path, host, host);
		int rc = _ipstack.connect(host, port, ssl, path, host, host, ietf_version);
		while (!_ipstack.isConnected())
		{
			if (_ipstack.isConnectFailed() || rc == -1)
			{
				Sleep(10);
				printf("Retry Connecting to %s:%d\n", host, port);
				rc = _ipstack.connect(host, port, ssl, path, host, host, ietf_version);
			}
			Sleep(500);
		}
		return rc;			
	}

	void CALLBACK MqttManager::_onTimerCallback(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime)
	{			
		MqttManager& _this = MqttManager::getInstance();
		_this._onTimer();			
	}		

	void MqttManager::_connect()
	{			
		if (NULL == _config)
			return;

		int rc = 0;
		bool ok = true;

		if (!_ipstack.isConnected())
		{
			if (_mqttClient.isConnected())
			{					
				_mqttClient.disconnect();
			}				
			rc = _connectIPStack();
		}			
		if (rc != 0 || !_ipstack.isConnected() || _ipstack.isConnectFailed())
		{
			printf("IPStack(%s:%d%s) Connectiion failed.\n", _config->host, _config->port, _config->path);
			ok = false;
		}		
		if (ok)
		{
			if (!_mqttClient.isConnected())
			{
				MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
				data.MQTTVersion = MQTT_VERSION;
				data.clientID.cstring = (char*)_config->clientID;
				data.username.cstring = (char*)_config->username;
				data.password.cstring = (char*)_config->password;
				rc = _mqttClient.connect(data);			
			}
			if (rc != 0 || !_mqttClient.isConnected())
			{
				printf("MQTTClient(%s:%d%s) Connectiion failed.\n", _config->host, _config->port, _config->path);
				ok = false;
			}									
		}
		if (ok)
		{
			printf("Connect to MQTTClient(%s:%d%s) successfully\n", _config->host, _config->port, _config->path);
			_curState = Connected;
			if (!_connectedCallback.empty())
			{
				_connectedCallback(_config);
			}
		}
	}

	void MqttManager::_onTimer()
	{
		__try
		{
			if (!_ipstack.isConnected() || !_mqttClient.isConnected())
			{
				if (_curState != Disconnected && !_disconnectedCallback.empty())
				{
					_disconnectedCallback(_config);
				}
				_curState = Disconnected;
				_connect();
			}
			else {
				_mqttClient.yield(MQTT_CLIENT_TIMEOUT);
			}
		}
		__except (StackTracer::ExceptionFilter(GetExceptionInformation()))
		{
			StackTracer_HandleException();
		}
	}

	void MqttManager::start(unsigned long intervalMs)
	{
		_curState = Started;
		_timerEvtID = SetTimer(NULL, TIMERID_MQTT, intervalMs, (TIMERPROC)&MqttManager::_onTimerCallback);
		CppWebsock::start();
	}

	void MqttManager::stop()
	{
		if (_timerEvtID != NONE_VALUE)
		{
			KillTimer(NULL, _timerEvtID);
			_timerEvtID = NONE_VALUE;
		}
		disconnect();
		CppWebsock::shutdown();
		_curState = Stoped;
	}

	void MqttManager::_makeConfig(const MQTTConfig& config)
	{
		if (_config)
		{
			delete _config;
		}

		_config = new MQTTConfig();
		_config->clientID = config.clientID;
		_config->host = config.host;
		_config->path = config.path;
		_config->port = config.port;
		_config->ssl = config.ssl;
		_config->username = config.username;
		_config->password = config.password;
	}

	void MqttManager::connect(const MQTTConfig& config)
	{
		__try
		{
			_makeConfig(config);
			_connect();
		}
		__except (StackTracer::ExceptionFilter(GetExceptionInformation()))
		{
			StackTracer_HandleException();
		}
	}

	void MqttManager::disconnect()
	{
		if (_ipstack.isConnected())
		{
			_ipstack.disconnect();
		}
		if (_mqttClient.isConnected())
		{
			_mqttClient.disconnect();
		}

		if (!_ipstack.isConnected() && !_mqttClient.isConnected())
			_curState = Disconnected;
	}

	int MqttManager::subscribe(const char* topicFilter, MQTT::QoS qos, MqttCallback messageArrived)
	{
		int rc = _mqttClient.subscribe(topicFilter, MQTT::QOS1, messageArrived);
		if (rc != 0)
			return rc;
		return 0;
	}

	void MqttManager::publish(const char* topicName, unsigned char* payload)
	{
		__try
		{
			MQTT::Message msg;
			msg.qos = MQTT::QOS1;
			msg.id = msg.retained = msg.dup = 0;
			msg.payload = payload;
			msg.payloadlen = strlen((char*)msg.payload);
	//		printf("[MqttManager] publish topic:%s payload:%s\n", topicName, msg.payload);
			_mqttClient.publish(topicName, msg);
		}
		__except (StackTracer::ExceptionFilter(GetExceptionInformation()))
		{
			StackTracer_HandleException();
		}
	}
}