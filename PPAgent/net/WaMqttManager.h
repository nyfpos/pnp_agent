#pragma once
#include "..\pch.h"
#include "..\lib\mqtt\MQTTClient.h"
#include "..\lib\mqtt\CppWebsock.h"
#include "boost\function.hpp"
#include "..\util\StackTracer.h"

namespace wa
{
	struct MQTTConfig
	{
		const char* host;
		const char* path;
		int port;
		int ssl;
		const char* clientID;
		const char* username;
		const char* password;			
	};		

	class MqttManager
	{	
	public:
		typedef boost::function<void(MQTT::MessageData&)> MqttCallback;			

		static MqttManager& getInstance()
		{
			static MqttManager    instance; // Guaranteed to be destroyed.
			return instance;
		}
	private:	
		enum MqttManagerState
		{
			None = -1,
			Started,
			Connected,
			Disconnected,
			Stoped
		};
		MqttManagerState _curState;			
		int _timerEvtID;

		MQTTConfig* _config;
		CppWebsock _ipstack;
		MQTT::Client<CppWebsock, Countdown> _mqttClient;			
		boost::function<void(MQTTConfig const *const)> _connectedCallback;
		boost::function<void(MQTTConfig const *const)> _disconnectedCallback;

		void _makeConfig(const MQTTConfig& config);
		int _connectIPStack();
		void _onTimer();
		void _connect();			
		static void CALLBACK _onTimerCallback(HWND hwnd, UINT uMsg, UINT timerId, DWORD dwTime);
		//MqttManager(MqttManager const&) {}
		void operator=(MqttManager const&) {}
	public:

		MqttManager();
		~MqttManager();
		void start(unsigned long intervalMs);
		void stop();
		void connect(const MQTTConfig& config);
		void disconnect();
		int subscribe(const char* topicFilter, MQTT::QoS qos, MqttCallback messageArrived);
		void publish(const char* topicName, unsigned char* payload);

		void setConnectedCallback(boost::function<void(MQTTConfig const *const)> cb)
		{
			_connectedCallback = cb;
		}

		void setDisconnectedCallback(boost::function<void(MQTTConfig const *const)> cb)
		{
			_disconnectedCallback = cb;
		}

		bool isConnected()
		{
			return _ipstack.isConnected() && _mqttClient.isConnected();
		}
	};
}