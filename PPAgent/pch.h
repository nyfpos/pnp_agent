#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <map>

#include <boost/log/support/date_time.hpp>

#include "models/IModel.h"

typedef unsigned char Byte;
#define MAX_WEBSOCKET_RECV_SIZE 512

#define _MAX_MQTT_PACKET_SIZE_ 512
#define _MAX_MQTT_WRITE_BUFF_SIZE_ 512
#define _RX_BUFFER_SIZE_ 65536

#define MQTT_CLIENT_TIMEOUT 60*60*1000
#define MQTT_TIMER_INTERVAL 33

#define MQTT_VERSION 3

#define NONE_VALUE -1

#define TIMERID_MQTT 0
#define TIMERID_TEST 1

#define LOGGER_BUFFER_SIZE 512

#define LOGGING_FILE_NAME "PPAgent.log"
#define LOGGING_FORMAT(logging, expr) (\
expr::stream\
<<"["<< boost::log::expressions::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d %T") <<"]"\
<< ": <" << logging::trivial::severity\
<< "> " << expr::smessage)


//"[%TimeStamp%]: %Message%"
#define LOGGING_FILTER(logging, rule) logging::core::get()->set_filter rule;
#define LOGGING_RULE(logging) (logging::trivial::severity >= logging::trivial::trace)
#define LOGGING_MAKE_BUFFER(buffer, fmt)\
{\
memset(buffer, 0, LOGGER_BUFFER_SIZE);\
va_list args;\
va_start(args, fmt);\
vsprintf_s(buffer, LOGGER_BUFFER_SIZE, fmt, args);\
va_end(args);\
}

#define DB_CONNECT_STR "DSN=bwdb_Access;"

#include "util\logger.h"
#include "util\convert.h"